(ns jobtech-dev.datalog-tool.transfere
  (:require [clojure.core.async :as async :refer [<! >!]]
            [clojure.set :as set]
            [java-time.api :as jt]
            [taoensso.timbre :as log]))

(defn- datoms->map [datoms]
  (map
   #(let [[eid aid value tx-id added] ((juxt :e :a :v :tx :added) %)]
      {:eid eid :attr aid :value value :tx tx-id :added added}) datoms))

(defn- update-attrib [known-idents datom]
  (update datom :attr #(known-idents % %)))

(defn- get-new-idents [db-ident-name datoms]
  (->> (filter #(= db-ident-name (:attr %)) datoms)
       (map (juxt :eid :value))
       (into {})))

(defn system-tx? [datoms]
  (not
   (seq
    (filter
     (fn [{:keys [eid tx value]}] 
       (and
        (= eid tx)
        (inst? value)
        (jt/after? (jt/instant value) (jt/instant 0))))
     datoms))))

(defn- handle-tx-rec [known-idents finished remaining]
  (if (seq? remaining)
    (let [updated-remaining (map (partial update-attrib known-idents) remaining)
          updated-known-idents (merge
                                known-idents
                                (get-new-idents
                                 (:found-identity known-idents)
                                 updated-remaining))
          possibly-finished (group-by #(keyword? (:attr %)) updated-remaining)
          new-finished (vec (concat finished (possibly-finished true)))
          new-remaining (possibly-finished false)]
      (if (and
           (= (count known-idents) (count updated-known-idents))
           (= (count remaining) (count new-remaining)))
        (do
          (log/debug "Deadlock detected" new-remaining)
          {:error :unknown-idents
           :known-idents known-idents
           :datoms (vec (filter #(number? (:attr %)) remaining))})
        (recur updated-known-idents new-finished new-remaining)))
    {:known-idents known-idents :datoms finished :system-tx? (system-tx? finished)}))

(defn handle-tx
  ([{:keys [t data]}]
   (if (= t 0)
     (let [datoms (datoms->map data)
           identity-eid (filterv #(= (:eid %) (:attr %)) datoms)]
       (if (= 1 (count identity-eid))
         (let [db-ident-datom (first identity-eid)
               db-ident-id (:eid db-ident-datom)
               db-ident-name (:value db-ident-datom)
               known-idents {:found-identity db-ident-name
                             db-ident-id db-ident-name}
               result (handle-tx-rec known-idents [] datoms)
               message {:t t
                        :system-tx? (:system-tx? result)
                        :known-idents (:known-idents result)
                        :remaining (count datoms)}]
           (if (= :db/ident db-ident-name)
             (log/info message)
             (log/warn (assoc message :renamed :db/ident)))
           (if (some? (:error result))
             (do
               (log/error :t t known-idents result)
               {:error :unrecoverable
                :t t
                :known-idents known-idents
                :result result})
             (assoc result :known-t 0 :t 0)))
         (log/error :t t :identity-eid identity-eid)))
     (do
       (log/error :t-not-zero t)
       {:error :expected-t-zero :t t})))
  ([{:keys [known-idents known-t]} {:keys [t data]}]
   (let [result (handle-tx-rec known-idents [] (datoms->map data))]
     (if-let [error (:error result)]
       (if (< known-t (dec t))
         (do
           (log/debug :t t :known-t known-t "failed, can retry")
           {:error error
            :t t
            :known-t known-t
            :result result})
         (let [response {:error :unrecoverable
                         :t t
                         :known-t known-t
                         :result result}]
           (log/error response "Identifier used before it's definition.")
           response))
       (let [old-known-keys (set (vals known-idents))
             new-known-keys (set (vals (:known-idents result)))
             new-keys (set/difference new-known-keys old-known-keys)
             info-message (cond-> {:t t :known-t known-t
                                   :system-tx? (:system-tx? result)}
                            (< 0 (count new-keys)) (assoc :new-keys new-keys))
             response (merge info-message result)]
         (log/trace response)
         (log/info info-message)
         response)))))

(defn infer-tx-data
  ([pre-processed-transactions]
   (let [user-transactions (map :datoms (drop-while :system-tx? pre-processed-transactions))]
     (log/debug user-transactions)
     (infer-tx-data [] {} user-transactions))
   )
  ([ready-transactions seen-eids pre-processed-transactions]
   (if-let [datoms (first pre-processed-transactions)]
     (let [tx-ids (set (map :tx datoms))
           eid-groups (group-by :eid datoms)
           tx-eids (set (keys eid-groups))
           new-seen-eids (reduce
                          (fn visited-eid [eids eid]
                            (assoc eids eid (- eid)))
                          seen-eids
                          tx-eids)]
       (if (= 1 (count tx-ids))
         (log/trace tx-ids)
         (log/warn :unusal-number-of-tx-ids tx-ids))
       (log/info :tx tx-ids)
       (let [tx-entries (apply concat (mapv
                                       (fn [[eid datoms]]
                                         (let [updated
                                               (mapv
                                                (fn [{:keys [attr value tx added]}]
                                                               (let [op (if added :db/add :db/retract)
                                                                     tmp-eid (if (= eid tx) "datomic.tx" (seen-eids eid (- eid)))]
                                                                 [op tmp-eid attr value])) datoms)]
                                           updated))
                                       eid-groups))]

         (recur (conj ready-transactions tx-entries) new-seen-eids (rest pre-processed-transactions))))
     (do
       (log/info :done (count ready-transactions))
       ready-transactions))))

(defn handle-tx-range
  ([tx-range] (let [result (handle-tx (first tx-range))
                    finished-datoms [{:system-tx? (:system-tx? result) :datoms (:datoms result)}]
                    state (dissoc result :datoms)]
                (handle-tx-range finished-datoms state (rest tx-range))))
  ([completed-datoms state tx-range]
   (if-let [next-tx (first tx-range)]
     (let [result (handle-tx state next-tx)]
       (if (some? (:error result))
         (do
           (log/error result)
           result)
         (let [{:keys [datoms known-idents t known-t new-keys system-tx?]} result
               new-state {:known-idents known-idents
                          :known-t (if (= (inc known-t) t) t known-t)}
               message (cond-> (assoc new-state :system-tx? system-tx?)
                         new-keys (assoc :new-keys new-keys))]
           (log/trace message)
           (log/debug (update-vals message (fn [v] (if (coll? v) (count v) v))))
           (recur (conj completed-datoms {:system-tx? system-tx? :datoms datoms}) new-state (rest tx-range)))))
     completed-datoms)))

(defn ^:export tx-pipeline [_conn number-of-threads src-ch]
  (let [dest-ch (async/chan 100)
        done-ch (async/chan)
        transactor (fn transactor [data result-ch]
                     (async/go
                       (if (= (:t data) 55)
                         (do
                           (async/timeout 1000)
                           (>! done-ch {:error "GOT FIVE!!!!!"})
                           (async/close! done-ch)
                           (async/close! src-ch)
                           (async/close! dest-ch)
                           (async/close! result-ch))
                         (do
                           (>! result-ch data)
                           (async/close! result-ch)))))]
  ;  (async/pipeline-blocking )
    (async/pipeline-async number-of-threads dest-ch transactor src-ch)
    (async/go-loop [[result total] [(<! dest-ch) 0]]
      (if (some? result)
        (let [new-total (inc total)]
;          (log/debug :tx result)
          (when (zero? (mod new-total 100))
            (log/debug :processed new-total))
          (recur [(<! dest-ch) new-total]))
        (do
          (>! done-ch {:completed total})
          (async/close! done-ch))))
    done-ch))