(ns jobtech-dev.datalog-tool.source
  (:require [taoensso.timbre :as log]
            [datomic.client.api.async :as datomic]))

(defmulti reader :type)

(defmethod reader :string
  [source]
  (let [unread (atom true)]
    (fn string-reader [& _cfg]
      (if @unread
        (do
          (log/debug "Setting message to read.")
          (reset! unread false)
          {:read :ok
           :data (:data source)})
        {:read :done}))))


(defmethod reader :datomic-tx
 [source]
  (let [transactions (datomic/tx-range (:db source) {})]
    (fn datomic-tx-reader [& _cfg]
      {:read :ok :data transactions}
      )))