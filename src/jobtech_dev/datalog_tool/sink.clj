(ns jobtech-dev.datalog-tool.sink
  (:require [taoensso.timbre :as log]))

(defmulti writer :type)

(defmethod writer :atom-vec
  [sink]
  (let [state (atom :open)]
    (fn write-data
      [command & data]
      (log/debug command)
      (if (not= :closed @state)
        (case command
          :done (do (reset! state :closed)
                    {:ok :closed})
          :write (do
                   (swap! (:sink sink)
                          #(reduce conj % data))
                   {:ok :written})
          {:error :command
           :command command})
        {:error :closed}))))