(ns jobtech-dev.datalog-tool.core
  (:require
   [taoensso.timbre :as log]
   [jobtech-dev.datalog-tool.source :as source]
   [jobtech-dev.datalog-tool.sink :as sink]))

(defn- copy-reader->writer [reader writer]
  (loop [data (reader)]
    (case (:read data)
      :ok (do (writer :write (:data data))
              (recur (reader)))
      :done (writer :done))))

(defn ^:export copy [source sink]
  (log/info "Copy" (:name source) '-> (:name sink))
  (let [reader (source/reader source)
        writer (sink/writer sink)]
    (copy-reader->writer reader writer)))
