(ns jobtech-dev.datalog-tool.core-test
  (:require [clojure.test :refer [is deftest testing]]
            [jobtech-dev.datalog-tool.core :as dtc]))

(deftest copy-mem-test
  (testing "Copy in memory"
    (let [out (atom [])]
      (dtc/copy {:type :string :data "text"}
                {:type :atom-vec :sink out})
      (is (= ["text"] @out)))))