(ns jobtech-dev.datalog-tool.sink-test
  (:require [clojure.test :refer [is deftest testing]]
            [jobtech-dev.datalog-tool.sink :as dt-source]))
  
  (deftest writer-mem-test
    (testing "writer :atom-vec"
      (let [writer-sink (atom [])
            writer (dt-source/writer {:type :atom-vec :sink writer-sink})]
        (is (= {:ok :written} (writer :write "text")))
        (is (= ["text"] @writer-sink))
        (is (= {:ok :written} (writer :write true 42)))
        (is (= ["text" true 42] @writer-sink))
        (is (= {:error :command
                :command :bob} (writer :bob)))
        (is (= {:ok :closed} (writer :done)))
        (is (= {:error :closed} (writer :done)))
        (is (= {:error :closed} (writer :write nil)))))) 