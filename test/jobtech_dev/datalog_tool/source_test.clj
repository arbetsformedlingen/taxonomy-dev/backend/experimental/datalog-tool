(ns jobtech-dev.datalog-tool.source-test
  (:require [clojure.test :refer [is deftest testing]]
            [datomic.client.api :as datomic]
            [jobtech-dev.datalog-tool.source :as dt-sink]))

(deftest reader-mem-test
  (testing "reader :string"
    (let [reader (dt-sink/reader {:type :string :data "text"})
          first-read (reader)]
      (is (= {:read :ok :data "text"} first-read))
      (is (= {:read :done} (reader))))))

(def datomic-local-cfg
  {:server-type :datomic-local
   :storage-dir :mem
   :system "testing"})

(def datomic-local-client
  (datomic/client datomic-local-cfg))

(def simple-schema
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2001-01-01"}
             {:db/ident :name
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/many
              :db/doc "A name amongst many."}]})

(defn- datomic-setup [db-cfg]
  (datomic/create-database datomic-local-client db-cfg)
  (let [conn (datomic/connect datomic-local-client db-cfg)]
    (datomic/transact conn simple-schema)
    (datomic/transact conn {:tx-data [{:db/id "datomic.tx"
                                       :db/txInstant #inst "2002-01-01"}
                                      {:name "bob"}]})))
(defn- datomic-teardown [db-cfg]
  (datomic/delete-database datomic-local-client db-cfg))

(deftest reader-datomic-test
  (testing "Read transactions from datomic"
    (let [db-cfg {:db-name "ethics-gradient"}]
      (datomic-setup db-cfg)
      (let [conn (datomic/connect datomic-local-client db-cfg)
            db (datomic/db conn)
            reader (dt-sink/reader {:type :datomic-tx :db db})]
        (is (= nil (reader))))
      (datomic-teardown db-cfg))))
