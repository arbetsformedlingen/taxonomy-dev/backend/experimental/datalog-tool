(ns jobtech-dev.datalog-tool.transfere-test
  (:require [clojure.core.async :as async :refer [<! <!! >! >!!]]
            [clojure.test :refer [deftest is testing]]
            [datomic.client.api :as datomic]
            [java-time.api :as jt]
            [datomic.client.api.async :as das]
            [jobtech-dev.datalog-tool.transfere :as transfere]))

(def datomic-local-cfg
  {:server-type :datomic-local
   :storage-dir :mem
   :system "testing"})

(def datomic-local-client
  (datomic/client datomic-local-cfg))

(def simple-schema
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2001-01-01"}
             {:db/ident :account
              :db/valueType :db.type/string
              :db/unique :db.unique/identity
              :db/cardinality :db.cardinality/one}]})
(def simple-data
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2001-01-02"}
             {:account "BurgerBob"}
             {:account "Linda"}
             {:account "fishdr"}]})
(def additional-schema
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2002-01-01"}
             {:db/ident :cash
              :db/valueType :db.type/long
              :db/cardinality :db.cardinality/one}
             {:db/ident :note
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/one}
             {:db/ident :name
              :db/valueType :db.type/string
              :db/cardinality :db.cardinality/many
              :db/doc "A name amongst many."}]})
(def additional-data
  {:tx-data [{:db/id "datomic.tx"
              :note "A TX Note"
              :db/txInstant #inst "2002-01-02"}
             {:account "Louise"
              :cash 4773
              :note "Not ill-gotten"}
             {:db/id [:account "BurgerBob"]
              :name "Bob Belcher"
              :cash 32
              :note "Incoming balance"}]})
(def additional-retract
  {:tx-data [{:db/id "datomic.tx"
              :note "Demand ID in the future."
              :db/txInstant #inst "2002-01-03"}
             [:db/retractEntity [:account "Louise"]]]})
(def additional-update
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2002-01-04"}
             {:db/id [:account "BurgerBob"]
              :cash 43}]})
(def additional-cas
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2002-01-05"}
             [:db/cas [:account "BurgerBob"] :cash 43 17]]})
(def additional-replace
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2002-01-06"}
             {:db/id [:account "fishdr"]
              :account "Fishoeder"}]})
(def additional-more
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2002-01-07"}
             {:db/id [:account "Fishoeder"]
              :note "Found in the second livingrooom couch."
              :cash 30000000}]})
(def excisable-schema
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2003-01-01"}
              {:db/ident :relation
              :db/valueType :db.type/ref
              :db/cardinality :db.cardinality/many}]})
(def excisable-data
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2003-01-02"}
             {:account "Felix"
              :relation [:account "Fishoeder"]}
             ]})
(def excisable-update
  {:tx-data [{:db/id "datomic.tx"
              :db/txInstant #inst "2003-01-03"}
             {:db/id [:account "Fishoeder"]
              :relation [:account "Felix"]}]})
#_(def excisable-excision
  {:tx-data [[:db/excise [:account "Felix"]]]})

(defn- datomic-setup [db-cfg]
  (datomic/create-database datomic-local-client db-cfg)
  (let [conn (datomic/connect datomic-local-client db-cfg)]
    (datomic/transact conn simple-schema)
    (datomic/transact conn simple-data)
    (datomic/transact conn additional-schema)
    (datomic/transact conn additional-data)
    (datomic/transact conn additional-retract)
    (datomic/transact conn additional-update)
    (datomic/transact conn additional-cas)
    (datomic/transact conn additional-replace)
    (datomic/transact conn additional-more)))

(defn- datomic-teardown [db-cfg]
  (datomic/delete-database datomic-local-client db-cfg))

(deftest handle-tx-test
  (testing "Read transactions from datomic"
    (let [db-source-cfg {:db-name "fate-amenable-to-change"}]
      (try
        (datomic-setup db-source-cfg)
        (let [conn-source (datomic/connect datomic-local-client db-source-cfg)
              source-tx (datomic/tx-range conn-source {#_#_:start (jt/java-date 1)})
              processed-tx (transfere/handle-tx-range source-tx)]
          (is (= 15 (count processed-tx)))
          (is (= [1] (transfere/infer-tx-data processed-tx)))
          #_(is (= [["bob"]] (datomic/q {:query '[:find ?attr ?val ?tx ?added
                                                :where
                                                [13194139533312 ?attr ?val ?tx ?added]]
                                       :args [(datomic/db conn-source)]})))
          
          #_(is (= {} (map (juxt :e :a :v :tx :added) (:data (first source-tx)))))
         #_ (is (= {} (transfere/handle-tx-range source-tx))))
        (finally
          (datomic-teardown db-source-cfg))))))

(deftest transact-test
  (testing "Read transactions from datomic"
    (let [db-source-cfg {:db-name "fate-amenable-to-change"}
          db-dest-cfg {:db-name "ethics-gradient"}]
      (try
        (datomic-setup db-source-cfg)
        (datomic/create-database datomic-local-client db-dest-cfg)
        (let [conn-source (datomic/connect datomic-local-client db-source-cfg)
              conn-dest (datomic/connect datomic-local-client db-dest-cfg)]
          (is (= [["Bob Belcher"]] (datomic/q {:query '[:find ?name
                                                :where
                                                [?e :name ?name]]
                                       :args [(datomic/db conn-source)]})))
          #_(is (= [] (datomic/db conn-dest)))
#_          (is (= [["bob"]] (datomic/q {:query '[:find ?name
                                                :where
                                                [?e :name ?name]]
                                       :args [(datomic/db conn-dest)]})))
          (let [source-tx (das/tx-range conn-source {#_#_:start (jt/java-date 1)})
                waitable (transfere/tx-pipeline conn-dest 16 source-tx)]
            (is (= {:completed 15}
                   (<!! waitable)))
            (is (nil? (<!! waitable)))
            #_(async/go
              
              (is (= simple-data (<! waitable)))
              (async/close! source-async)
              (is (= simple-data (<! waitable))))
            #_(is (= {:completed 8} (<!! (:result waitable))))
            #_(is (= nil tx-done))
            #_(is (= [] (<!! (das/db-stats (das/db conn-dest)))))
            #_(is (= {} (update tx-schema :data #(map (juxt :e :a :v :tx :added) %))))
            #_(is (= {} (update tx-bob :data #(map (juxt :e :a :v :tx :added) %))))
            #_(let [db-schema (<!! (das/transact conn-dest (:data tx-schema)))]
                (is (= [] (<!! (das/db-stats (das/db conn-dest)))
                       #_(map (juxt :e :a :v :tx :added) (:tx-data db-schema)))))
            #_(let [db-bob (<!! (das/transact conn-dest (:data tx-bob)))]
                (is (= []
                       (map (juxt :e :a :v :tx :added) (:tx-data db-bob)))))
            #_(is (= [] (<!! (das/db-stats (das/db conn-dest)))))
            #_(is (= [["bob"]] (<!! (das/q
                                     {:query '[:find ?name
                                               :where [?e :name ?name]]
                                      :args [(das/db conn-dest)]}))))))
        (finally
          (datomic-teardown db-source-cfg)
          (datomic-teardown db-dest-cfg))))))
