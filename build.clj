(ns build
  (:require [clojure.tools.build.api :as b]))

(def lib 'jobtech-dev/datalog-tool)
(def version-major 0)
(def version-minor 1)
(def version-rev (b/git-count-revs nil))
(def version (format "%d.%d.%s" version-major version-minor version-rev))

(def build-dir "target")

(def class-dir (format "%s/classes" build-dir))

(def basis (b/create-basis {:project "deps.edn"}))

(def src-dirs ["src"])

(defn clean [_]
  (b/delete {:path build-dir}))

(defn ^:export jar [_]
  (clean nil)
  (b/write-pom {:class-dir class-dir
                :lib lib
                :version version
                :basis basis
                :src-dirs src-dirs})
  (b/copy-dir {:src-dirs src-dirs
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
           :jar-file (format "%s/%s-%s.jar" build-dir (name lib) version)}))